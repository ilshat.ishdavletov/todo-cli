# Todo List Application

## Description

This is a command-line application, based on [Rust](https://www.rust-lang.org), for managing your task list. You can add, mark tasks as completed, and delete them from your list.

## Usage

Commands for interacting with the application:

Add a task to the list:
```
cargo run add "Write code"
```

Mark a task as completed:
```
cargo run complete "Write code"
```

Remove a task from the list:
```
cargo run remove "Write code"
```

## Installation

First, make sure you have Rust installed. Then clone this repository and run using cargo:

```
git clone https://gitlab.com/ilshat.ishdavletov/todo-cli.git
cd todo-cli
cargo run -- <command> "<your task>"
```