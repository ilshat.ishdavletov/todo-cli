use std::collections::HashMap;
use std::io::Read;

pub struct Todo {
    // use rust built in HashMap to store key - val pairs
    pub map: HashMap<String, bool>,
}

impl Todo {
    pub fn new() -> Result<Todo, std::io::Error> {
        let mut f = std::fs::OpenOptions::new()
            .write(true)
            .create(true)
            .read(true)
            .open("db.md")?;
        let mut content = String::new();
        f.read_to_string(&mut content)?;
        let map: HashMap<String, bool> = content
            .lines()
            .map(|line| line.splitn(2, '\t').collect::<Vec<&str>>())
            .map(|v| (v[0], v[1]))
            .map(|(k, v)| (String::from(v), k.contains('x')))
            .collect();
        Ok(Todo { map })
    }
    pub fn insert(&mut self, key: String) {
        self.map.insert(key, false);
    }

    pub fn save(self) -> Result<(), std::io::Error> {
        let mut content = String::new();
        let mut x: char;
        for (key, val) in self.map {
            if val == false {
                x = ' ';
            } else {
                x = 'x';
            };
            content.push_str(&format!("- [{}] \t{}\n", x, key));
        }
        std::fs::write("db.md", content)
    }

    pub fn complete(&mut self, key: &String) -> Option<()> {
        match self.map.get_mut(key) {
            Some(v) => Some(*v = true),
            None => None,
        }
    }

    pub fn remove(&mut self, key: &String) -> Option<bool> {
        self.map.remove(key)
    }
}
